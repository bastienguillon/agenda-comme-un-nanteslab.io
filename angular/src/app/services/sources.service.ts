import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ISource } from '../models/sources.model';

@Injectable()
export class SourcesService {
	constructor(private http: HttpClient) { }

	public getSources(): Observable<ISource[]> {
		return this.http.get('https://backup.agenda-comme-un-nantes.org/sources.json').pipe(
			map(sources => {
				return sources as ISource[];
			})
		);
	}
}
