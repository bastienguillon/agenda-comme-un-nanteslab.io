import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IEvent } from '../models/events.model';
import { flatten, uniq } from 'lodash';

@Injectable()
export class EventsService {
	constructor(private http: HttpClient) { }

	public getEvents(): Observable<IEvent[]> {
		return this.http.get('https://backup.agenda-comme-un-nantes.org/events.json').pipe(
			map(sources => {
				return sources as IEvent[];
			})
		);
	}

	public getSourcesNames(): Observable<string[]> {
		return this.getEvents().pipe(
			map(events => {
				return uniq(flatten(events.map(event => event.sourceKeys.map(source => source.libelle))));
			})
		);
	}
}


// https://backup.agenda-comme-un-nantes.org/events.json