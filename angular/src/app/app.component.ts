import { Component, OnInit } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import listPlugin from '@fullcalendar/list';
import * as moment from 'moment';
import { IEvent } from './models/events.model';
import { EventsService } from './services/events.service';

export interface ICalendarEvent {
	date?: string;
	start?: string;
	end?: string;
	rendering?: string;
	color?: string;
	title: string;
	event: IEvent;
}

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	public title = 'Transiscope';
	public sidenavIsOpened = false;
	public calendarPlugins = [dayGridPlugin, listPlugin];

	public startsOn: moment.Moment;
	public endsOn: moment.Moment;

	public sources: string[];
	public selectedSources: string[];

	public events: ICalendarEvent[];
	public allEvents: ICalendarEvent[];

	constructor(
		private eventsService: EventsService
	) { }

	public ngOnInit(): void {
		this.eventsService.getSourcesNames().subscribe(sources => {
			this.sources = sources;
			this.selectedSources = [...this.sources];
		});
		this.eventsService.getEvents().subscribe(events => {
			// this.events = [{ title: 'event 1', date: '2019-11-08' }, { title: 'event 3', date: '2019-11-04' }];
			this.events = events.map(event => {
				return {
					title: event.title,
					date: moment(event.start).format('YYYY-MM-DD'),
					event
				} as ICalendarEvent;
			});
			this.allEvents = [...this.events];
		});
	}

	public onStartsOnChanged(): void {
		if (this.startsOn == null) {
			this.events.forEach(event => {
				event.color = null;
			});
		}

		this.events
			.filter(event => moment(event.date, 'YYYY-MM-DD').isBefore(this.startsOn))
			.forEach(event => {
				event.color = '#aaa';
			});
		this.events = [...this.events];
	}

	public onEndsOnChanged(): void {
		if (this.endsOn == null) {
			this.events.forEach(event => {
				event.color = null;
			});
		}

		this.events
			.filter(event => moment(event.date, 'YYYY-MM-DD').isAfter(this.endsOn))
			.forEach(event => {
				event.color = '#eee';
			});
		this.events = [...this.events];
	}

	public onSourcesChanged(event: any): void {

	}
}
