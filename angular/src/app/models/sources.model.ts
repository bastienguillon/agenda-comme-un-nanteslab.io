export enum SourceType {
	fb = 'fb'
}

export interface ISource {
	key: string;
	libelle: string;
	home: string;
	type: SourceType;
}
