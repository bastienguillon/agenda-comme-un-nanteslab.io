import { ISource } from './sources.model';

export interface IEvent {
	sourceKeys: ISource[];
	title: string;
	description: string;
	start: string;
	end: string;
	place: string;
	url: string;
	id: string;
}
