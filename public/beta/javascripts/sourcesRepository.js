// sources
(function (root) {
    var fetchTimeout = 2000;

    function clean(source) {
        if (source.key.search(/http/) !== -1) {
            source.url = source.key;
            source.home = source.home ? source.home : source.key;
        } else {
            source.url = 'https://www.facebook.com/' + source.key;
            source.home = source.home ? source.home : 'https://www.facebook.com/' + source.key + '/events';
        }

        return source;
    }

    function fetchSources(url) {
        return agenda.utils.fetchWithTimeout(url, fetchTimeout)
            .then(function(response) {
                return response.json();
            })
            .then(function(sources) {
                return sources.map(clean).filter(function (source) {
                    return source.key && source.libelle;
                });
            })
            .then(sourceSet);
    }

    function sourceSet(sources) {
        var sortedSources = sources
            // deduplicate
            .filter(function(value, index, self) { return self.indexOf(value) === index; })
            .sort(function(a, b) { return a.libelle > b.libelle; });
        var mapByKey = sortedSources.reduce(function(map, source) {
            map[source.key] = source.libelle;
            return map;
        }, {});

        function getList() {
            return sortedSources;
        }
        function getKeys() {
            return Object.getOwnPropertyNames(mapByKey);
        }
        function searchByKey(key) {
            return mapByKey[key];
        }
        function getMapByKey() {
            return mapByKey;
        }

        return {
            getList: getList,
            getKeys: getKeys,
            searchByKey: searchByKey,
            getMapByKey: getMapByKey
        }
    }

    var sources = {
        fetch: fetchSources
    };

    root.agenda = root.agenda || {};
    root.agenda.sources = sources;

}(this));
