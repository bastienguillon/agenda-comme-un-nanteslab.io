// sources picker
(function (root) {
    var _paq = _paq !== undefined ? _paq : null;

    function initJs() {
        return Promise.all([]);
    }

    function initCss(shadowRoot) {
        return Promise.all([
            root.agenda.utils.loadCSS(root.agenda.utils.libDirectory + 'stylesheets/custom-sourcePicker.css', shadowRoot),
            root.agenda.utils.loadCSS('https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css', shadowRoot)
        ]);
    }

    function init(shadowRoot) {
        return Promise.all([initCss(shadowRoot), initJs()]);
    }

    function getSelectedSources(shadowRoot) {
        var sources = [];
        shadowRoot.querySelectorAll('.sourcesFilteringCheckbox:checked').forEach(function(sourceElement) {
            sources.push(sourceElement.value);
        });

        return sources;
    }

    function getSourceHtml(source, selected) {
        return '<div class="custom-control custom-checkbox">' +
            '<input type="checkbox" class="custom-control-input sourcesFilteringCheckbox" ' +
            'id="source-filtering-' + source.key + '" ' +
            'value="' + source.key + '"' +
            (selected ? 'checked' : '') +
            '>' +
            '<label class="custom-control-label" for="source-filtering-' + source.key + '"><span>' + source.libelle + '</span></label>' +
            ' (<a href="' + source.home + '" target="_blank">site</a>)' +
            '</div>';
    }

    function render(shadowRoot, binding, allSources, selectedSources) {
        var updateCalendar = function(sources) {
            document.querySelectorAll(binding).forEach(function(element) {
                element.sourceChange(sources);
            })
        };

        return init(shadowRoot).then(function() {
            allSources.getList().forEach(function(source) {
                shadowRoot.innerHTML += getSourceHtml(source, selectedSources.getKeys().includes(source.key));

            });

            var checkboxCollection = shadowRoot.querySelectorAll('.sourcesFilteringCheckbox');

            checkboxCollection.forEach(function(checkbox) {
                checkbox.addEventListener('click', function () {
                    updateCalendar(getSelectedSources(shadowRoot));
                });
            });

            shadowRoot.querySelector('#source-deselectAll').addEventListener('click', function () {
                checkboxCollection.forEach(function(e) {
                    e.checked = false;
                });
                updateCalendar(getSelectedSources(shadowRoot));

                return false;
            });

            shadowRoot.querySelector('#source-selectAll').addEventListener('click', function () {
                checkboxCollection.forEach(function(e) {
                    e.checked = true;
                });
                updateCalendar(getSelectedSources(shadowRoot));

                return false;
            });
        });
    }

    function requestSources(url) {
        var errorHandler = function(textStatus) {
            _paq && _paq.push(['trackEvent', 'sources', 'backup', textStatus]);
        };

        return agenda.sources.fetch(url)
            .then(function(sourceSet) {
                if (sourceSet.getList().length == 0) {
                    errorHandler('empty');
                }

                return sourceSet;
            })
            .catch(function(err) {
                return errorHandler(err.message);
            });
    }

    customElements.define('agenda-comme-un-sources-picker', class extends HTMLElement {
        constructor() {
            super();

            var shadowRoot = this.attachShadow({mode: 'open'});

            var allSelector = document.createElement('div');
            allSelector.setAttribute('class', 'select-all-sources');
            allSelector.innerHTML = '<a href="#" id="source-selectAll">Tout sélectionner</a> / <a href="#"  id="source-deselectAll">Tout enlever</a>';

            shadowRoot.appendChild(allSelector);
        }
        connectedCallback() {
            var sourcePicker = this;

            requestSources(sourcePicker.getAttribute('sources'))
                .then(function(sources) {
                    return render(sourcePicker.shadowRoot, sourcePicker.getAttribute('binding'), sources, sources);
                });
        }
    });
}(this));
