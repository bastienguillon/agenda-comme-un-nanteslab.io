// utils
(function (root) {

    // source : https://davidwalsh.name/fetch-timeout
    function fetchWithTimeout(url, fetchTimeout) {
        var didTimeOut = false;

        return new Promise(function (resolve, reject) {
            var timeout = setTimeout(function () {
                didTimeOut = true;
                reject(new Error('Request timed out'));
            }, fetchTimeout);

            fetch(url, {cache: "no-cache"})
                .then(function (response) {
                    // Clear the timeout as cleanup
                    clearTimeout(timeout);
                    if (!didTimeOut) {
                        resolve(response);
                    }
                })
                .catch(function (err) {
                    // Rejection already happened with setTimeout
                    if (didTimeOut) return;
                    // Reject with error
                    reject(err);
                });
        })
    }

    function loadScript(source, beforeEl, async = true, defer = true) {
        return new Promise(function (resolve, reject) {
            var script = document.createElement('script');
            var prior = beforeEl || document.getElementsByTagName('script')[0];

            script.async = async;
            script.defer = defer;

            function onloadHander(_, isAbort) {
                if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
                    script.onload = null;
                    script.onreadystatechange = null;
                    script = undefined;

                    if (isAbort) { reject(); } else { resolve(); }
                }
            }

            script.onload = onloadHander;
            script.onreadystatechange = onloadHander;

            script.src = source;
            prior.parentNode.insertBefore(script, prior);
        });
    }

    function loadCSS(source, container) {
        return new Promise(function(resolve) {
            var link = document.createElement('link');
            link.rel = 'stylesheet';
            link.href = source;
            (container.head && container.head.appendChild(link)) || container.appendChild(link);
            link.onload = function() {
                resolve();
            };
        });
    }

    var utils = {
        fetchWithTimeout: fetchWithTimeout,
        loadScript: loadScript,
        libDirectory: document.currentScript.src.replace('javascripts/agenda.js', ''),
        loadCSS: loadCSS
    };

    root.agenda = root.agenda || {};
    root.agenda.utils = utils;

}(this));

(function(root) {
    root.agenda.init = function() {
        return Promise.all([
            agenda.utils.loadScript(agenda.utils.libDirectory + '/javascripts/eventsRepository.js'),
            agenda.utils.loadScript(agenda.utils.libDirectory + '/javascripts/sourcesRepository.js'),
            agenda.utils.loadCSS('https://use.fontawesome.com/releases/v5.0.6/css/all.css', document),
            agenda.utils.loadCSS('https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css', document),
            agenda.utils.loadCSS(agenda.utils.libDirectory + 'stylesheets/custom-agenda.css', document)
        ]).then(function() {
            return agenda.utils.loadScript(agenda.utils.libDirectory + '/javascripts/sourcesPicker.js')
        }).then(function() {
            return agenda.utils.loadScript(agenda.utils.libDirectory + '/javascripts/calendarUI.js')
        });
    };

    agenda.init();
})(this);
