// calendar ui
(function (root) {
    var _paq = _paq !== undefined ? _paq : null;

    function initJs(prior) {
        return Promise.all([
            root.agenda.utils.loadScript(root.agenda.utils.libDirectory + 'javascripts/lib/fullcalendar/core/main.min.js', prior),
            root.agenda.utils.loadScript(root.agenda.utils.libDirectory + 'javascripts/lib/jquery.min.js', prior),
            root.agenda.utils.loadScript('https://unpkg.com/popper.js@1', prior),
            root.agenda.utils.loadScript('https://unpkg.com/tippy.js@4', prior)
        ]).then(function() {
            return Promise.all([
                root.agenda.utils.loadScript('https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', prior),
                root.agenda.utils.loadScript(root.agenda.utils.libDirectory + 'javascripts/lib/fullcalendar/bootstrap/main.min.js', prior),
                root.agenda.utils.loadScript(root.agenda.utils.libDirectory + 'javascripts/lib/fullcalendar/daygrid/main.min.js', prior),
                root.agenda.utils.loadScript(root.agenda.utils.libDirectory + 'javascripts/lib/fullcalendar/list/main.min.js', prior),
                root.agenda.utils.loadScript(root.agenda.utils.libDirectory + 'javascripts/lib/fullcalendar/core/locales/fr.js', prior)
            ]);
        });
    }

    function initCss(shadowRoot) {
        return Promise.all([
            root.agenda.utils.loadCSS('https://use.fontawesome.com/releases/v5.0.6/css/all.css', shadowRoot),
            root.agenda.utils.loadCSS(root.agenda.utils.libDirectory + 'javascripts/lib/fullcalendar/core/main.min.css', shadowRoot),
            root.agenda.utils.loadCSS(root.agenda.utils.libDirectory + 'javascripts/lib/fullcalendar/daygrid/main.min.css', shadowRoot),
            root.agenda.utils.loadCSS(root.agenda.utils.libDirectory + 'javascripts/lib/fullcalendar/list/main.min.css', shadowRoot),
            root.agenda.utils.loadCSS(root.agenda.utils.libDirectory + 'javascripts/lib/fullcalendar/bootstrap/main.min.css', shadowRoot),
            root.agenda.utils.loadCSS(root.agenda.utils.libDirectory + 'stylesheets/custom-agenda.css', shadowRoot),
            root.agenda.utils.loadCSS('https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css', shadowRoot)
        ]);
    }

    function init(shadowRoot) {
        return Promise.all([initCss(shadowRoot), initJs(shadowRoot.lastChild)]);
    }

    function isMobile() {
        const mobileWidth = 768;
        return screen.width < mobileWidth;
    }

    function render(shadowRoot, eventsFetcher) {
        return init(shadowRoot).then(function() {
            var calendarContainer = document.createElement('div');
            calendarContainer.setAttribute('id', 'calendarContainer');
            shadowRoot.appendChild(calendarContainer);

            var calendarComponent = new FullCalendar.Calendar(calendarContainer, {
                plugins: [ 'dayGrid', 'list', 'bootstrap' ],
                themeSystem: 'bootstrap',
                header: {
                    left: (isMobile() ? 'title' : 'prev,next today'),
                    center: (isMobile() ? 'dayGridMonth,listMonth' : 'title'),
                    right: (isMobile() ? 'prev,next' :'dayGridMonth,listMonth')
                },
                defaultView: (isMobile() ? 'listMonth' : 'dayGridMonth'),
                eventLimit: true, // allow "more" link when too many events
                events: eventsFetcher,
                locale: 'fr',
                contentHeight: 'auto',
                buttonText: {
                    month: 'Calendrier',
                    list: 'Liste'
                },
                timezone: 'local',
                customButtons: {
                },
                eventClick: function (info) {
                    info.jsEvent.preventDefault(); // don't let the browser navigate
                    var event = info.event;
                    var hosts = event.extendedProps.hosts;
                    var description = event.extendedProps.description;
                    var image = event.extendedProps.image;
                    var sharers = event.extendedProps.sharers;

                    var content =
                        '<div class="card">' +
                            '<div class="card-header">' + event.title + '</div>' +
                            '<div class="card-body">' +
                                (hosts.length > 0 ? '<div class="event-hosts">Organisé par : <b>' + hosts.join(', ') + '</b></div>' : '') +
                                (sharers.length > 0 ? '<div class="event-sharer">Diffusé par : <b>' + sharers.join(', ') + '</b></div>' : '') +
                                '<div class="event-description">' + description +
                                    (image ? '<img src="' + image.src + '">' : '') +
                                '</div>' +
                                '<a href="' + event.url + '" target="_blank">En savoir plus</a>' +
                            '</div>' +
                        '</div>';

                    var tip = info.el._tippy || tippy(info.el, {
                        content: content,
                        duration: 0,
                        interactive: true,
                        theme: 'commeun',
                        trigger: 'click manual'
                    });
                    tip.show();
                    return false;
                }
            });

            calendarComponent.render();
            return calendarComponent;
        });
    }

    function requestSources(url) {
        var errorHandler = function(textStatus) {
            _paq && _paq.push(['trackEvent', 'sources', 'backup', textStatus]);
        };

        return agenda.sources.fetch(url)
            .then(function(sourceSet) {
                if (sourceSet.getList().length == 0) {
                    errorHandler('empty');
                }

                return sourceSet;
            })
            .catch(function(err) {
                return errorHandler(err.message);
            });
    }

    function requestEvents(url, sources) {
        var errorHandler = function(textStatus) {
            _paq && _paq.push(['trackEvent', 'events', 'backup', textStatus]);
        };

        return agenda.events.fetch(url, sources)
            .then(function(events) {
                if (events.length == 0) {
                    return errorHandler('empty');
                }

                return events;
            })
            .catch(function(err) {
                return errorHandler(err.message);
            });
    }

    var state = {
        allEvents: [],
        filteredEvents: [],
        calendarComponent: null
    };

    function getEventsFilteredBySources(sources) {
        return agenda.events.filterEvents(state.allEvents, sources);
    }

    function eventsFetcher(fetchInfo, successCallback) {
        return successCallback(state.filteredEvents);
    }

    customElements.define('agenda-comme-un-calendar', class extends HTMLElement {
        constructor() {
            super();

            var shadowRoot = this.attachShadow({mode: 'open'});

            var style = document.createElement('style');
            shadowRoot.appendChild(style);
        }

        connectedCallback() {
            var calendar = this;

            var sourcePromise = requestSources(calendar.getAttribute('sources'));

            sourcePromise.then(function(sources) {
                return requestEvents(calendar.getAttribute('events'), sources)
                .then(function(events){
                    return {
                        sources,
                        events
                    };
                });
            }).then(function(results) {
                state.allEvents = results.events;
                state.filteredEvents = getEventsFilteredBySources(results.sources.getKeys());

                return render(
                    calendar.shadowRoot,
                    eventsFetcher
                ).then(function(calendarComponent) {
                    state.calendarComponent = calendarComponent;
                });
            });
        }

        sourceChange(sources) {
            state.filteredEvents = getEventsFilteredBySources(sources);
            state.calendarComponent.refetchEvents();
        }
    });
}(this));
