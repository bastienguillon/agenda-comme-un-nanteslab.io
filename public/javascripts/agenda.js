// utils
(function (root) {

    // source : https://davidwalsh.name/fetch-timeout
    function fetchWithTimeout(url, fetchTimeout) {
        var didTimeOut = false;

        return new Promise(function (resolve, reject) {
            var timeout = setTimeout(function () {
                didTimeOut = true;
                reject(new Error('Request timed out'));
            }, fetchTimeout);

            fetch(url, {cache: "no-cache"})
                .then(function (response) {
                    // Clear the timeout as cleanup
                    clearTimeout(timeout);
                    if (!didTimeOut) {
                        resolve(response);
                    }
                })
                .catch(function (err) {
                    // Rejection already happened with setTimeout
                    if (didTimeOut) return;
                    // Reject with error
                    reject(err);
                });
        })
    }

    var utils = {
        fetchWithTimeout: fetchWithTimeout
    };

    root.agenda = root.agenda || {};
    root.agenda.utils = utils;

}(this));


// events
(function (root) {
    var eventsUrl = 'https://backup.agenda-comme-un-nantes.org/events.json';
    var backupUrl = 'https://grappe.io/data/api/5c961ef44a490f002e4598f6-agenda-comme-un-nantes-events-all';
    var fetchTimeout = 2000;

    function clean(event) {
        if ((Array.isArray(event.fbPages))) {
            event.sharers = event.fbPages.map(function (fbPage) {
                if (typeof fbPage.url === 'string') {
                    fbPage.id = fbPage.url.split('/').pop();
                } else {
                    fbPage.id = 'unknown';
                    fbPage.url = '';
                }

                if (typeof fbPage.title !== 'string') {
                    fbPage.title = '-';
                }
                return fbPage;
            });
        } else if (Array.isArray(event.source)) {
            event.sharers = event.source.map(function (source) {
                return {
                    id: source.key,
                    url: source.key,
                    title: source.name
                };
            });
        } else if (Array.isArray(event.sourceKeys)) {
            event.sharers = event.sourceKeys.map(function (source) {
                return {
                    id: source.key || source,
                    url: source.key || source,
                    title: source.libelle || source
                };
            });
        } else {
            event.sharers = [];
        }

        event.hosts = (Array.isArray(event.hosts)) ? event.hosts.map(function (host) {
            return (typeof host === 'string') ? host : '';
        }) : [];

        var textfields = ['title', 'description', 'place'];
        textfields.forEach(function (field) {
            if (typeof event[field] !== 'string') {
                event[field] = '';
            }
        });

        var dateFields = ['start', 'end'];
        dateFields.forEach(function (field) {
            if (typeof event[field] !== 'string' || !event[field].match(/20[0-9]{2}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}.*/)) {
                event[field] = '';
            }
        });

        var isValidImage = (Array.isArray(event.image) && event.image.length > 0);
        event.image = isValidImage ? {src: Array.isArray(event.image[0].value) ? event.image[0].value[0] : event.image[0].value} : null;

        return event;
    }

    function fetchEvents(url) {
        return agenda.utils.fetchWithTimeout(url, fetchTimeout)
            .then(function (response) {
                return response.json();
            })
            .then(function (events) {
                return events.map(clean);
            });
    }

    function filterEvents(events, sources) {
        return events.filter(function(event) {
            return (event.fbPages && event.fbPages.some(function(sharer) {
                return sources.includes(sharer.id);
            }))
            || (event.sourceKeys && event.sourceKeys.some(function(sharer) {
                return sources.includes(sharer.key || sharer);
            }));
        });
    }

    var events = {
        fetch: fetchEvents,
        filterEvents: filterEvents,
        eventsUrl: eventsUrl,
        backupUrl: backupUrl
    };

    root.agenda = root.agenda || {};
    root.agenda.events = events;

}(this));


// sources
(function (root) {
    var sourcesUrl = 'https://backup.agenda-comme-un-nantes.org/sources.json';
    var backupSourceUrl = 'https://grappe.io/data/api/5c33d692942ea300128d0f89-agenda-commun-nantes-sources';
    var fetchTimeout = 2000;

    function clean(source) {
        if (source.key.search(/http/) !== -1) {
            source.url = source.key;
            source.home = source.home ? source.home : source.key;
        } else {
            source.url = 'https://www.facebook.com/' + source.key;
            source.home = source.home ? source.home : 'https://www.facebook.com/' + source.key + '/events';
        }

        return source;
    }

    function fetchSources(url) {
        return agenda.utils.fetchWithTimeout(url, fetchTimeout)
            .then(function(response) {
                return response.json();
            })
            .then(function(sources) {
                return sources.map(clean).filter(function (source) {
                    return source.key && source.libelle;
                });
            })
            .then(sourceSet);
    }

    function sourceSet(sources) {
        var sortedSources = sources
            // deduplicate
            .filter(function(value, index, self) { return self.indexOf(value) === index; })
            .sort(function(a, b) { return a.libelle > b.libelle; });
        var mapByKey = sortedSources.reduce(function(map, source) {
            map[source.key] = source.libelle;
            return map;
        }, {});

        function getList() {
            return sortedSources;
        }
        function getKeys() {
            return Object.getOwnPropertyNames(mapByKey);
        }
        function searchByKey(key) {
            return mapByKey[key];
        }
        function getMapByKey() {
            return mapByKey;
        }

        return {
            getList: getList,
            getKeys: getKeys,
            searchByKey: searchByKey,
            getMapByKey: getMapByKey
        }
    }

    var sources = {
        fetch: fetchSources,
        sourcesUrl: sourcesUrl,
        backupSourceUrl: backupSourceUrl
    };

    root.agenda = root.agenda || {};
    root.agenda.sources = sources;

}(this));
